$(document).ready(function () {
    configurePopup('name');
    configurePopup('website');
    configurePopup('phone');
    configurePopup('address');

    configureMobileProfileEdition();

    configureReviewStars(1);
    configureReviewStars(2);
    configureReviewStars(3);
    configureReviewStars(4);
    configureReviewStars(5);
});

function configurePopup(property) {
    $('.content').on('click', '.content__edit--' + property, function () {
        if (property === 'name') {
            $('#first-name-desktop').val($('.content__info--first-name').text());
            if ($('#first-name-desktop').val()) {
                $('.mdl-textfield--first-name-desktop').addClass('is-dirty');
            }

            $('#last-name-desktop').val($('.content__info--last-name').text());
            if ($('#last-name-desktop').val()) {
                $('.mdl-textfield--last-name-desktop').addClass('is-dirty');
            }
        } else {
            $('#' + property + '-desktop').val($('.content__info--' + property).text());

            if ($('#' + property + '-desktop').val()) {
                $('.mdl-textfield--' + property + '-desktop').addClass('is-dirty');
            }
        }

        $('.content__edit-popup--' + property).show();
    });

    $('.content').on('click', '.content__edit-button-cancel--' + property, function () {
        $('.content__edit-popup--' + property).hide();
    });

    $('.content').on('click', '.content__edit-button-save--' + property, function () {
        if (property === 'name') {
            $('.content__info--first-name').text($('#first-name-desktop').val());
            $('.content__info--last-name').text($('#last-name-desktop').val());
        } else {
            $('.content__info--' + property).text($('#' + property + '-desktop').val());
        }
        $('.content__edit-popup--' + property).hide();
    });
}

function configureMobileProfileEdition() {
    $('.content').on('click', '.content__edit--mobile', function () {
        configureMobileProfilePropertyEdition('first-name');
        configureMobileProfilePropertyEdition('last-name');
        configureMobileProfilePropertyEdition('website');
        configureMobileProfilePropertyEdition('phone');
        configureMobileProfilePropertyEdition('address');

        $('.content-editable').show();
        $('.content-static').hide();
    });

    $('.content').on('click', '.content-editable__button--cancel', function () {
        $('.content-editable').hide();
        $('.content-static').show();
    });

    $('.content').on('click', '.content-editable__button--save', function () {
        $('.content__info--first-name').text($('#first-name-mobile').val());
        $('.content__info--last-name').text($('#last-name-mobile').val());
        $('.content__info--website').text($('#website-mobile').val());
        $('.content__info--phone').text($('#phone-mobile').val());
        $('.content__info--address').text($('#address-mobile').val());

        $('.content-editable').hide();
        $('.content-static').show();
    });
}

function configureMobileProfilePropertyEdition(property) {
    $('#' + property + '-mobile').val($('.content__info--' + property).text());

    if ($('#' + property + '-mobile').val()) {
        $('.mdl-textfield--' + property + '-mobile').addClass('is-dirty');
    }
}

function configureReviewStars(starNumber) {
    $('#profile-header').on('click', '.profile-header__reviews-star--' + starNumber + '-star', function () {
        for (var i = 1; i <= 5; i++) {
            if (i <= starNumber) {
                $('.profile-header__reviews-star--' + i + '-star').removeClass('ion-android-star-outline');
                $('.profile-header__reviews-star--' + i + '-star').addClass('ion-android-star');
            } else {
                $('.profile-header__reviews-star--' + i + '-star').removeClass('ion-android-star');
                $('.profile-header__reviews-star--' + i + '-star').addClass('ion-android-star-outline');
            }
        }
    });
}