$(document).ready(() => {
	$('#passwordResetRequestButton').click(() => {
		$('#error').text('');
		$('#loading').show();

		var data = {
			email: $('#email').val()
		}

	    $.ajax({
		    type: "POST",
		    url: "http://52.14.104.204:8081/passwordReset",
		    data: JSON.stringify(data),
		    contentType: "application/json; charset=utf-8",
		    dataType: "json",
//			crossDomain: true,
			xhrFields: {
		       withCredentials: true
		    },
			complete: function(data) {
				console.log(data.responseText)
				if (data.status === 200) {
					window.location.href = '../';
				} else {
					$('#error').text(JSON.parse(data.responseText).text);
					$('#loading').hide();
				}
			}
		});
	});

	$('#setNewPasswordButton').click(() => {
		$('#error').text('');
		$('#loading').show();

		var data = {
			password: $('#password').val(),
			passwordResetToken: getURLParameter('token')
		}
		
		console.log(data)

	    $.ajax({
		    type: "POST",
		    url: "http://52.14.104.204:8081/setNewPassword",
		    data: JSON.stringify(data),
		    contentType: "application/json; charset=utf-8",
		    dataType: "json",
//			crossDomain: true,
			xhrFields: {
		       withCredentials: true
		    },
			complete: function(data) {
				console.log(data.responseText)
				if (data.status === 200) {
					window.location.href = '../';
				} else {
					$('#error').text(JSON.parse(data.responseText).text);
					$('#loading').hide();
				}
			}
		});
	});
});

function getURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
	
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}
