$(document).ready(() => {
	$('#linkedinLogin').click(() => {
		$('#error').text('');
		$('#loading').show();

	    $.ajax({
		    type: "GET",
		    url: "http://52.14.104.204:8080/linkedinLogin",
		    data: '',
		    contentType: "application/json; charset=utf-8",
		    dataType: "json",
			crossDomain: true,
			xhrFields: {
		       withCredentials: true
		    }
		});
	});

	$('#loginButton').click(() => {
		$('#error').text('');
		$('#loading').show();

		var data = {
			email: $('#email').val(),
			password: $('#password').val()
		}

	    $.ajax({
		    type: "POST",
		    url: "http://52.14.104.204:8080/login",
		    data: JSON.stringify(data),
		    contentType: "application/json; charset=utf-8",
		    dataType: "json",
			xhrFields: {
		       withCredentials: true
		    },
			complete: function(data) {
				if (data.status === 200) {
					window.location.href = '../profile.html';
				} else {
					$('#error').text(data.responseText);
					$('#loading').hide();
				}
			}
		});
	});

	$('#signupButton').click(() => {
		$('#error').text('');
		$('#loading').show();

		var data = {
			email: $('#email').val(),
			password: $('#password').val()
		}

	    $.ajax({
		    type: "POST",
		    url: "http://52.14.104.204:8080/signup",
		    data: JSON.stringify(data),
		    contentType: "application/json; charset=utf-8",
		    dataType: "json",
		    complete: function(data) {
				if (data.status === 200) {
					window.location.href = '../signup_second_step.html';
				} else {
					$('#error').text(data.responseText);
					$('#loading').hide();
				}
			}
		});
	});

	$('#logoutButton').click(() => {
	    $.ajax({
		    type: "POST",
		    url: "http://52.14.104.204:8080/logout",
		    data: '',
		    contentType: "application/json; charset=utf-8",
		    dataType: "json",
			xhrFields: {
		       withCredentials: true
		    },
			complete: function(data) {
				if (data.status === 200) {
					window.location.href = './';
				} else {
					$('#error').text(data.responseText);
				}
			}
		});
	});

	$('#saveButton').click(() => {
		$('#error').text('');
		$('#loading').show();

		var data = {
			name: $('#name').val(),
			address: $('#address').val(),
			email: $('#email').val(),
			phone: $('#phone').val()
		}

	    $.ajax({
		    type: "POST",
		    url: "http://52.14.104.204:8080/updateUser",
		    data: JSON.stringify(data),
		    contentType: "application/json; charset=utf-8",
		    dataType: "json",
		    complete: function(data) {
				$('#loading').hide();
				if (data.status === 200) {
					window.location.href = '../profile.html';
				} else {
					$('#error').text(data.responseText);
				}
			}
		});
	});
});

