package handler

import (
	"bytes"
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"

	"bitbucket.org/WillRoque/Willian_Roque_Test/webserver/model"
	"golang.org/x/oauth2"
)

var (
	oauthStateString = "tesTEStesTEStesTESt"
	oauthConf        = &oauth2.Config{
		ClientID:     "78ge515wsfxzg0",
		ClientSecret: "rJBAmAtXeB9pK9bM",
		RedirectURL:  "http://52.14.104.204:8080/linkedinLoginCallback",
	}
)

// Sends the user's data to the API so he can be signed up,
// if his data is ok, he is signed up and redirected to the profile page,
// otherwise and error is shown.
func Signup(w http.ResponseWriter, r *http.Request) {
	user := DecodeUserFromResponseBody(r.Body)

	backendReqBody, err := json.Marshal(user)
	if err != nil {
		panic(err)
	}

	backendResBody, statusCode := SendPostRequestToApi("signup", string(backendReqBody))
	if statusCode == http.StatusOK {
		responseData, err := ioutil.ReadAll(backendResBody)
		if err != nil {
			panic(err)
		}

		user.ApiAccessToken = string(responseData)
		user.StartSession(w)
	} else {
		jsonError := DecodeJsonErrorFromResponseBody(backendResBody)
		w.WriteHeader(jsonError.Code)
		w.Write([]byte(jsonError.Text))
	}
}

// Authenticates the user in the API and starts a session on the browser,
// if his data is ok, he is signed up and redirected to the profile page,
// otherwise and error is shown.
func Login(w http.ResponseWriter, r *http.Request) {
	user := DecodeUserFromResponseBody(r.Body)

	backendReqBody, err := json.Marshal(user)
	if err != nil {
		panic(err)
	}

	backendResBody, statusCode := SendPostRequestToApi("login", string(backendReqBody))
	if statusCode == http.StatusOK {
		responseData, err := ioutil.ReadAll(backendResBody)
		if err != nil {
			panic(err)
		}

		user.ApiAccessToken = string(responseData)
		user.StartSession(w)
	} else {
		jsonError := DecodeJsonErrorFromResponseBody(backendResBody)
		w.WriteHeader(jsonError.Code)
		w.Write([]byte(jsonError.Text))
	}
}

// Creates the login URL and redirects the user to the Linkedin login page.
func LinkedinLogin(w http.ResponseWriter, r *http.Request) {
	Url, err := url.Parse("https://www.linkedin.com/oauth/v2/authorization")

	if err != nil {
		log.Fatal("Parse: ", err)
	}

	parameters := url.Values{}
	parameters.Add("client_id", oauthConf.ClientID)
	parameters.Add("redirect_uri", oauthConf.RedirectURL)
	parameters.Add("response_type", "code")
	parameters.Add("state", oauthStateString)

	Url.RawQuery = parameters.Encode()
	url := Url.String()

	http.Redirect(w, r, url, http.StatusTemporaryRedirect)
}

// This method is called once the user accepts or refuses to login with Linkedin.
// If he accepts, an access token is requested to the Linkedin API.
func LinkedinLoginCallback(w http.ResponseWriter, r *http.Request) {
	if r.URL.Query().Get("error") != "" {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	state := r.URL.Query().Get("state")
	if state != oauthStateString {
		fmt.Printf("invalid oauth state, expected '%s', got '%s'\n", oauthStateString, state)
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	// Requests the Linkedin API access token.
	requestUrl := "https://www.linkedin.com/oauth/v2/accessToken"
	form := url.Values{
		"grant_type":    {"authorization_code"},
		"code":          {r.URL.Query().Get("code")},
		"redirect_uri":  {oauthConf.RedirectURL},
		"client_id":     {oauthConf.ClientID},
		"client_secret": {oauthConf.ClientSecret},
	}

	// Receives the access token.
	body := bytes.NewBufferString(form.Encode())
	rsp, err := http.Post(requestUrl, "application/x-www-form-urlencoded", body)
	if err != nil {
		panic(err)
	}
	defer rsp.Body.Close()

	decoder := json.NewDecoder(rsp.Body)
	var token model.Token
	err = decoder.Decode(&token)
	if err != nil {
		panic(err)
	}

	// Sends the access token to the back-end
	// so it can get and save the user data in the database.
	backendReqBody := "{ \"linkedinToken\": \"" + token.AccessToken + "\" }"
	backendResBody, statusCode := SendPostRequestToApi("linkedinLogin", backendReqBody)

	if statusCode == http.StatusOK {
		u := DecodeUserFromResponseBody(backendResBody)
		u.StartSession(w)
		http.Redirect(w, r, "/profile.html", http.StatusFound)
	}

	http.Redirect(w, r, "/", http.StatusFound)
}

// Serves the static profile page to the browser if the user is logged in,
// otherwise, redirects to the initial page.
func Profile(w http.ResponseWriter, r *http.Request) {
	apiAccessToken := new(model.User).GetApiAccessToken(r)

	if apiAccessToken != "" {
		u := model.User{ApiAccessToken: apiAccessToken}
		backendReqUrl := "getUserData"
		backendReqBody, err := json.Marshal(u)
		if err != nil {
			panic(err)
		}

		backendResBody, _ := SendPostRequestToApi(backendReqUrl, string(backendReqBody))
		err = json.NewDecoder(backendResBody).Decode(&u)
		if err != nil {
			panic(err)
		}

		u.Password = ""
		t, _ := template.ParseFiles("./static/profile.html")
		t.Execute(w, u)
	} else {
		http.Redirect(w, r, "/", http.StatusFound)
	}
}

// Serves the profile page to the browser if the user is logged in,
// otherwise, redirects to the initial page.
func ProfileEdit(w http.ResponseWriter, r *http.Request) {
	apiAccessToken := new(model.User).GetApiAccessToken(r)

	if apiAccessToken != "" {
		u := model.User{ApiAccessToken: apiAccessToken}
		backendReqUrl := "getUserData"
		backendReqBody, err := json.Marshal(u)
		if err != nil {
			panic(err)
		}

		backendResBody, _ := SendPostRequestToApi(backendReqUrl, string(backendReqBody))
		err = json.NewDecoder(backendResBody).Decode(&u)
		if err != nil {
			panic(err)
		}

		u.Password = ""
		t, _ := template.ParseFiles("./static/profile_edit.html")
		t.Execute(w, u)
	} else {
		http.Redirect(w, r, "/", http.StatusFound)
	}
}

// Serves the second step sign-up page so the user can complete his registration.
func SignupSecondStep(w http.ResponseWriter, r *http.Request) {
	apiAccessToken := new(model.User).GetApiAccessToken(r)

	if apiAccessToken != "" {
		u := model.User{ApiAccessToken: apiAccessToken}
		backendReqUrl := "getUserData"
		backendReqBody, err := json.Marshal(u)
		if err != nil {
			panic(err)
		}

		backendResBody, _ := SendPostRequestToApi(backendReqUrl, string(backendReqBody))
		err = json.NewDecoder(backendResBody).Decode(&u)
		if err != nil {
			panic(err)
		}

		u.Password = ""
		t, _ := template.ParseFiles("./static/signup_second_step.html")
		t.Execute(w, u)
	} else {
		http.Redirect(w, r, "/", http.StatusFound)
	}
}

// Validates if the user is logged in and
// sends his changed data to be updated in the API.
func UpdateUser(w http.ResponseWriter, r *http.Request) {
	apiAccessToken := new(model.User).GetApiAccessToken(r)

	if apiAccessToken != "" {
		u := DecodeUserFromResponseBody(r.Body)
		u.ApiAccessToken = apiAccessToken
		backendReqUrl := "updateUser"
		backendReqBody, err := json.Marshal(u)
		if err != nil {
			panic(err)
		}

		backendResBody, statusCode := SendPostRequestToApi(backendReqUrl, string(backendReqBody))
		if statusCode == http.StatusOK {
			_, err := ioutil.ReadAll(backendResBody)
			if err != nil {
				panic(err)
			}
		} else {
			jsonError := DecodeJsonErrorFromResponseBody(backendResBody)
			w.WriteHeader(jsonError.Code)
			w.Write([]byte(jsonError.Text))
		}
	} else {
		http.Redirect(w, r, "/", http.StatusFound)
	}
}

// Logs the user out by clearing the cookie
// that holds his authentication information.
func Logout(w http.ResponseWriter, r *http.Request) {
	cookie := &http.Cookie{
		Name:   "go_test_user_session",
		Value:  "",
		Path:   "/",
		MaxAge: -1,
	}

	http.SetCookie(w, cookie)
	w.WriteHeader(http.StatusOK)
}
