package handler

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"io"
	"net/http"

	"bitbucket.org/WillRoque/Willian_Roque_Test/webserver/model"
)

// Sends a post request to the API and returns its body.
func SendPostRequestToApi(endpoint, body string) (rspBody io.ReadCloser, statusCode int) {
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}

	client := &http.Client{Transport: tr}
	rsp, err := client.Post("http://52.14.104.204:8081/"+endpoint, "application/json", bytes.NewBufferString(body))
	if err != nil {
		panic(err)
	}

	return rsp.Body, rsp.StatusCode
}

// Decodes the json body from an http response and parses it into a User model.
func DecodeUserFromResponseBody(body io.ReadCloser) model.User {
	var user model.User
	err := json.NewDecoder(body).Decode(&user)

	if err != nil {
		panic(err)
	}

	return user
}

// Decodes the json body from an http response and parses it into a JsonError model.
func DecodeJsonErrorFromResponseBody(body io.ReadCloser) model.JsonError {
	var jsonError model.JsonError
	err := json.NewDecoder(body).Decode(&jsonError)

	if err != nil {
		panic(err)
	}

	return jsonError
}
