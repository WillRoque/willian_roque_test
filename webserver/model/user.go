package model

import (
	"net/http"
	"time"

	"github.com/gorilla/securecookie"
)

var (
	UserCookieName = "go_test_user_session"
	cookieHandler  = securecookie.New(
		[]byte{6, 181, 102, 59, 162, 151, 232, 44, 181, 203, 36, 60, 245, 231, 95,
			115, 15, 165, 164, 167, 119, 194, 227, 72, 54, 3, 249, 69, 139, 231, 241, 194,
			2, 66, 68, 143, 99, 79, 213, 190, 157, 223, 162, 77, 57, 199, 63, 182, 87, 54,
			208, 232, 2, 66, 173, 45, 103, 46, 231, 34, 175, 220, 251, 140},
		[]byte{80, 114, 231, 86, 13, 201, 185, 126, 109, 183, 75, 56, 245, 109, 174,
			25, 64, 239, 160, 81, 131, 226, 155, 18, 165, 236, 71, 195, 31, 2, 143, 174})
)

type User struct {
	Name           string    `json:"name"`
	Address        string    `json:"address"`
	Email          string    `json:"email"`
	Phone          string    `json:"phone"`
	LinkedinToken  string    `json:"linkedinToken`
	ApiAccessToken string    `json:"apiAccessToken"`
	Password       string    `json:"password"`
	SignupDateTime time.Time `json:"signupDateTime"`
}

// Creates a cookie with the user's access tokens so he can use the API.
func (u *User) StartSession(w http.ResponseWriter) {
	value := map[string]string{
		"ApiAccessToken": u.ApiAccessToken,
		"LinkedinToken":  u.LinkedinToken,
	}

	if encoded, err := cookieHandler.Encode(UserCookieName, value); err == nil {
		cookie := &http.Cookie{
			Name:    UserCookieName,
			Value:   encoded,
			Path:    "/",
			Expires: time.Now().Add(1 * time.Hour),
		}

		http.SetCookie(w, cookie)
	}
}

// Looks for the cookie that holds the information about the logged in user,
// if it finds it, returns the api access token, if it doesn't, returns an empty string.
func (u *User) GetApiAccessToken(r *http.Request) (email string) {
	if cookie, err := r.Cookie(UserCookieName); err == nil {
		cookieValue := make(map[string]string)

		if err = cookieHandler.Decode(UserCookieName, cookie.Value, &cookieValue); err == nil {
			email = cookieValue["ApiAccessToken"]
		}
	}

	return email
}
