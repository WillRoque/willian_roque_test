package main

import (
	"log"
	"net/http"

	"bitbucket.org/WillRoque/Willian_Roque_Test/webserver/handler"
)

// Simple web server with the purpose to serve the static files,
// this way, being completely separate from the API.
func main() {
	http.Handle("/", http.FileServer(http.Dir("./static/")))
	http.Handle("/signup", http.HandlerFunc(handler.Signup))
	http.Handle("/signup_second_step.html", http.HandlerFunc(handler.SignupSecondStep))
	http.Handle("/login", http.HandlerFunc(handler.Login))
	http.Handle("/logout", http.HandlerFunc(handler.Logout))
	http.Handle("/profile.html", http.HandlerFunc(handler.Profile))
	http.Handle("/profile_edit.html", http.HandlerFunc(handler.ProfileEdit))
	http.Handle("/linkedinLogin", http.HandlerFunc(handler.LinkedinLogin))
	http.Handle("/linkedinLoginCallback", http.HandlerFunc(handler.LinkedinLoginCallback))
	http.Handle("/updateUser", http.HandlerFunc(handler.UpdateUser))

	// Use this for production (https)
	//log.Fatal(http.ListenAndServeTLS(":8080", "../https_keys/cert.pem", "../https_keys/key.pem", nil))

	log.Fatal(http.ListenAndServe(":8080", nil))
}
