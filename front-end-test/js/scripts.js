var currentPage = 'about';
var pages = ['about', 'settings', 'option1', 'option2', 'option3'];

$(document).ready(function () {
    w3IncludeHTML();

    $('.login-form__button').on('click', function () {
        window.location.href = './profile.html';
    });

    $('#profile-header').on('click', '.profile-header__log-out-button', function () {
        window.location.href = './index.html';
    });

    configureMenuItems('about');
    configureMenuItems('settings');
    configureMenuItems('option1');
    configureMenuItems('option2');
    configureMenuItems('option3');
});

function configureMenuItems(item) {
    $('#profile-header').on('click', '.profile-header__menu-ul-li-span--' + item, function () {
        changePage(currentPage, item);

        $('.profile-header__menu-ul-li-span--' + currentPage).removeClass('profile-header__menu-ul-li-span--active');
        $('.profile-header__menu-ul-li-span--' + item).addClass('profile-header__menu-ul-li-span--active');
    });
}

function changePage(from, to) {
    $.ajax({
        url: to + '.html',
        dataType: 'html',
        success: function (data) {
            if (!isMobile()) {
                $('.content--main').html(data);
                componentHandler.upgradeDom();
            } else {
                $('.content--temp').html(data);
                $('.content--temp').removeClass('invisible');

                if (pages.indexOf(to) > pages.indexOf(from)) {
                    // slides to the left
                    $('.content--temp').css('left', '100%');
                    $('.content--main').animate({ left: '-100%' }, {
                        duration: 300,
                        complete: function ()  {
                            switchContentMainAndTempClasses();
                            $('.content--temp').html('');
                            componentHandler.upgradeDom();
                        }
                    });
                    $('.content--temp').animate({ left: '0' }, { duration: 250 });
                } else {
                    // slides to the right
                    $('.content--temp').css('left', '-100%');
                    $('.content--main').animate({ left: '100%' }, {
                        duration: 300,
                        complete: function ()  {
                            switchContentMainAndTempClasses();
                            $('.content--temp').html('');
                            componentHandler.upgradeDom();
                        }
                    });
                    $('.content--temp').animate({ left: '0' }, { duration: 250 });
                }
            }

            currentPage = to;
        }
    });
}

function isMobile() {
    return $(window).width() < 768;
}

function switchContentMainAndTempClasses() {
    $('.content--main').addClass('content--remove-main')
        .addClass('content--add-temp')
        .addClass('invisible');

    $('.content--remove-main').removeClass('content--main')
        .removeClass('content--remove-main');

    $('.content--temp').addClass('content--main')
        .removeClass('content--temp');

    $('.content--add-temp').addClass('content--temp')
        .removeClass('content--add-temp');
}