package model

type JsonError struct {
	Code int    `json:"code"`
	Text string `json:"text"`
}
