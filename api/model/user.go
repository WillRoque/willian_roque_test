package model

import (
	"encoding/json"
	"net/http"
	"time"

	"bitbucket.org/WillRoque/Willian_Roque_Test/api/db"
)

type User struct {
	Id                 uint64    `json:"id"`
	Name               string    `json:"name"`
	Address            string    `json:"address"`
	Email              string    `json:"email"`
	Phone              string    `json:"phone"`
	LinkedinId         string    `json:"linkedinId`
	LinkedinToken      string    `json:"linkedinToken`
	ApiAccessToken     string    `json:"apiAccessToken"`
	Password           string    `json:"password"`
	PasswordResetToken string    `json:"passwordResetToken"`
	SignupDateTime     time.Time `json:"signupDateTime"`
}

type LinkedinResponse struct {
	Id            string `json:"id"`
	EmailAddress  string `json:"emailAddress"`
	FormattedName string `json:"formattedName"`
	Location      struct {
		Name string `json:"name"`
	} `json:"location"`
}

func (u *User) Save() {
	if u.SignupDateTime.IsZero() {
		u.SignupDateTime = time.Now()
	}

	var keyName string
	var keyValue interface{}

	if u.Id != 0 {
		keyName = "id"
		keyValue = u.Id
	} else if u.Email != "" {
		keyName = "email"
		keyValue = u.Email
	} else {
		keyName = "api_access_token"
		keyValue = u.ApiAccessToken
	}

	if u.Id != 0 {
		_, err := db.Con.Exec(`UPDATE user
		SET name = ?, address = ?, email = ?, phone = ?, linkedin_token = ?, api_access_token = ?, password = ?, password_reset_token = ? 
		WHERE `+keyName+` = ?`,
			u.Name, u.Address, u.Email, u.Phone, u.LinkedinToken, u.ApiAccessToken, u.Password, u.PasswordResetToken, keyValue)
		if err != nil {
			panic(err)
		}
	} else {
		_, err := db.Con.Exec(`INSERT INTO user
		(name, address, email, phone, linkedin_id, linkedin_token, api_access_token, password, password_reset_token, signup_datetime)
		values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`,
			u.Name, u.Address, u.Email, u.Phone, u.LinkedinId, u.LinkedinToken, u.ApiAccessToken,
			u.Password, u.PasswordResetToken, u.SignupDateTime)
		if err != nil {
			panic(err)
		}
	}
}

func (u *User) LoadUserByLinkedinId() User {
	return loadUser("linkedin_id", u.LinkedinId)
}

func (u *User) LoadUserByEmail() User {
	return loadUser("email", u.Email)
}

func (u *User) LoadUserByApiAccessToken() User {
	return loadUser("api_access_token", u.ApiAccessToken)
}

func (u *User) LoadUserByPasswordResetToken() User {
	return loadUser("password_reset_token", u.PasswordResetToken)
}

func loadUser(keyName, keyValue string) User {
	rows, err := db.Con.Query(`SELECT id, name, address, email, phone, linkedin_id, 
			linkedin_token, api_access_token, password, password_reset_token, signup_datetime
		FROM user WHERE `+keyName+` = ? LIMIT 1`, keyValue)
	if err != nil {
		panic(err)
	}

	var id uint64
	var name, address, email, phone, linkedin_id, linkedin_token, api_access_token, password, password_reset_token string
	var signup_datetime time.Time

	if rows.Next() {
		err = rows.Scan(&id, &name, &address, &email, &phone, &linkedin_id,
			&linkedin_token, &api_access_token, &password, &password_reset_token, &signup_datetime)
		if err != nil {
			panic(err)
		}

		return User{Id: id,
			Name:               name,
			Address:            address,
			Email:              email,
			Phone:              phone,
			LinkedinId:         linkedin_id,
			LinkedinToken:      linkedin_token,
			ApiAccessToken:     api_access_token,
			Password:           password,
			PasswordResetToken: password_reset_token,
			SignupDateTime:     signup_datetime}
	}

	return User{}
}

func (u *User) IsSavedOnDatabase() bool {
	dbUser := u.LoadUserByEmail()
	if len(dbUser.Email) == 0 {
		return false
	}

	return true
}

func (u *User) GetLinkedinInfoFromAPI() {
	url := "https://api.linkedin.com/v1/people/~:(id,formatted-name,email-address,location:(name))?format=json"
	url += "&oauth2_access_token=" + u.LinkedinToken

	res, err := http.Get(url)
	if err != nil {
		panic(err)
	} else {
		defer res.Body.Close()

		var linkedinRes LinkedinResponse
		err = json.NewDecoder(res.Body).Decode(&linkedinRes)
		if err != nil {
			panic(err)
		}

		u.LinkedinId = linkedinRes.Id
		u.Email = linkedinRes.EmailAddress
		u.Name = linkedinRes.FormattedName
		u.Address = linkedinRes.Location.Name
	}
}
