package main

import (
	"log"
	"net/http"

	"bitbucket.org/WillRoque/Willian_Roque_Test/api/db"
	"bitbucket.org/WillRoque/Willian_Roque_Test/api/routing"
	"github.com/rs/cors"
)

func main() {
	db.Connect()
	router := routing.NewRouter()

	c := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowCredentials: true,
	})
	handler := c.Handler(router)

	// Use this for production (https)
	//Flog.Fatal(http.ListenAndServeTLS(":8081", "../https_keys/cert.pem", "../https_keys/key.pem", handler))

	log.Fatal(http.ListenAndServe(":8081", handler))
}
