package handler

import (
	"crypto/rand"
	"encoding/json"
	"net/http"

	"bitbucket.org/WillRoque/Willian_Roque_Test/api/model"
	"github.com/badoux/checkmail"
)

func DecodeUserFromHttpRequest(r *http.Request) model.User {
	var user model.User
	err := json.NewDecoder(r.Body).Decode(&user)

	if err != nil {
		panic(err)
	}

	return user
}

func VerifyUserData(u model.User) model.JsonError {
	if err := checkmail.ValidateFormat(u.Email); err != nil {
		return model.JsonError{Code: http.StatusBadRequest, Text: "Invalid email"}
	}

	if len(u.LinkedinToken) == 0 && len(u.Password) < 6 {
		return model.JsonError{Code: http.StatusBadRequest, Text: "Please insert a password with at least 6 characters"}
	}

	return model.JsonError{}
}

func GenerateApiAccessToken() string {
	return GenerateRandomString(32)
}

func GenerateRandomString(strSize int) string {
	dictionary := "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
	var bytes = make([]byte, strSize)
	rand.Read(bytes)
	for k, v := range bytes {
		bytes[k] = dictionary[v%byte(len(dictionary))]
	}
	return string(bytes)
}
