package handler

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/WillRoque/Willian_Roque_Test/api/email"
	"bitbucket.org/WillRoque/Willian_Roque_Test/api/model"
	"github.com/badoux/checkmail"
	"golang.org/x/crypto/bcrypt"
)

// Receives a Linkedin access token to its API,
// if the user is loging in for the first time with Linkedin,
// his data is fetched with the Linkedin API, and then saved in the database,
// otherwise just authenticates him by his Linkedin ID,
// and returns his data to the client with a access token to this API.
func LinkedinLogin(w http.ResponseWriter, r *http.Request) {
	user := DecodeUserFromHttpRequest(r)

	if len(user.LinkedinToken) == 0 {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Linkedin access token missing"))
		return
	}

	// If this is the first time login in with Linkedin,
	// then load his information with the Linkedin API.
	user.GetLinkedinInfoFromAPI()
	var dbUser = user.LoadUserByLinkedinId()
	if dbUser.Id == 0 {
		// Doesn't save the email retrieved by the Linkedin API,
		// if there is already an acount registered with that email.
		userSameEmail := user.LoadUserByEmail()
		if userSameEmail.Id != 0 {
			user.Email = ""
		}

		user.ApiAccessToken = GenerateApiAccessToken()
		user.Save()
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(user)
	} else {
		dbUser.ApiAccessToken = GenerateApiAccessToken()
		dbUser.Save()
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(dbUser)
	}
}

// Saves the new user in the database,
// generates a access token to this API, and returns it to the client.
func Signup(w http.ResponseWriter, r *http.Request) {
	user := DecodeUserFromHttpRequest(r)

	if err := VerifyUserData(user); err.Code != 0 {
		w.WriteHeader(err.Code)
		json.NewEncoder(w).Encode(err)
		return
	}

	if user.IsSavedOnDatabase() {
		w.WriteHeader(http.StatusConflict)
		err := model.JsonError{Code: http.StatusConflict, Text: "Email already registered"}
		json.NewEncoder(w).Encode(err)
		return
	}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	if err != nil {
		panic(err)
	}

	user.Password = string(hashedPassword)
	user.ApiAccessToken = GenerateApiAccessToken()
	user.Save()
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(user.ApiAccessToken))
}

// Authenticates the user using his password,
// generates a access token to this API, and returns it to the client.
func Login(w http.ResponseWriter, r *http.Request) {
	user := DecodeUserFromHttpRequest(r)

	if err := VerifyUserData(user); err.Code != 0 {
		w.WriteHeader(err.Code)
		json.NewEncoder(w).Encode(err)
		return
	}

	var dbUser = user.LoadUserByEmail()

	// User not found, returns an error message to the client.
	if dbUser.Id == 0 {
		jsonErr := model.JsonError{Code: http.StatusNotFound, Text: "No user with the email " + user.Email + " is registered"}
		w.WriteHeader(jsonErr.Code)
		json.NewEncoder(w).Encode(jsonErr)
		return
	}

	// Compares if the entered password hash is the same as the stored in the database.
	// If the passwords aren't the same, returns an error message to the client.
	if err := bcrypt.CompareHashAndPassword([]byte(dbUser.Password), []byte(user.Password)); err != nil {
		jsonErr := model.JsonError{Code: http.StatusUnauthorized, Text: "Incorrect password"}
		w.WriteHeader(jsonErr.Code)
		json.NewEncoder(w).Encode(jsonErr)
		return
	}

	dbUser.ApiAccessToken = GenerateApiAccessToken()
	dbUser.Save()

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(dbUser.ApiAccessToken))
}

// Loads the user from the database using his access token as a key,
// and returns his data to the client.
func GetUserData(w http.ResponseWriter, r *http.Request) {
	user := DecodeUserFromHttpRequest(r)
	dbUser := user.LoadUserByApiAccessToken()

	if dbUser.ApiAccessToken == "" {
		jsonErr := model.JsonError{Code: http.StatusUnauthorized, Text: "Invalid API Access Token"}
		w.WriteHeader(jsonErr.Code)
		json.NewEncoder(w).Encode(jsonErr)
		return
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(dbUser)
}

// Saves the user's changed data in the database.
func UpdateUser(w http.ResponseWriter, r *http.Request) {
	user := DecodeUserFromHttpRequest(r)

	if user.Email == "" {
		jsonErr := model.JsonError{Code: http.StatusBadRequest, Text: "Email can not be empty"}
		w.WriteHeader(jsonErr.Code)
		json.NewEncoder(w).Encode(jsonErr)
		return
	}

	if err := checkmail.ValidateFormat(user.Email); err != nil {
		jsonErr := model.JsonError{Code: http.StatusBadRequest, Text: "Invalid email"}
		w.WriteHeader(jsonErr.Code)
		json.NewEncoder(w).Encode(jsonErr)
	}

	dbUser := user.LoadUserByApiAccessToken()
	if dbUser.ApiAccessToken == "" {
		jsonErr := model.JsonError{Code: http.StatusUnauthorized, Text: "Invalid API Access Token"}
		w.WriteHeader(jsonErr.Code)
		json.NewEncoder(w).Encode(jsonErr)
		return
	}

	// Checks if an user with this email is already registered
	var userWithThisEmail = user.LoadUserByEmail()
	if user.Email != dbUser.Email && userWithThisEmail.Id != 0 {
		err := model.JsonError{Code: http.StatusNotFound, Text: "Email already in use by another account"}
		w.WriteHeader(err.Code)
		json.NewEncoder(w).Encode(err)
		return
	}

	dbUser.Name = user.Name
	dbUser.Address = user.Address
	dbUser.Email = user.Email
	dbUser.Phone = user.Phone

	dbUser.Save()
	w.WriteHeader(http.StatusOK)
}

// Sends an email to the user with a link to reset his password.
func PasswordReset(w http.ResponseWriter, r *http.Request) {
	user := DecodeUserFromHttpRequest(r)

	if err := checkmail.ValidateFormat(user.Email); err != nil {
		jsonErr := model.JsonError{Code: http.StatusBadRequest, Text: "Invalid email"}
		w.WriteHeader(jsonErr.Code)
		json.NewEncoder(w).Encode(jsonErr)
		return
	}

	dbUser := user.LoadUserByEmail()
	if dbUser.Id == 0 {
		jsonErr := model.JsonError{Code: http.StatusBadRequest, Text: "No user registered with this email"}
		w.WriteHeader(jsonErr.Code)
		json.NewEncoder(w).Encode(jsonErr)
		return
	}

	dbUser.PasswordResetToken = GenerateRandomString(32)
	dbUser.Save()

	pwResetUrl := "http://52.14.104.204:8080/forgot_password.html?token=" + dbUser.PasswordResetToken
	emailBody := "Click on the link below to reset your password:<br/><br/><a href=\"" + pwResetUrl + "\">" + pwResetUrl + "</a>"
	email.SendPasswordRecoveryEmail(dbUser, emailBody)
	w.WriteHeader(http.StatusOK)
}

// Sets the new password for the user
// that used the reset password link sent to his email.
func SetNewPassword(w http.ResponseWriter, r *http.Request) {
	user := DecodeUserFromHttpRequest(r)

	dbUser := user.LoadUserByPasswordResetToken()
	if dbUser.Id == 0 {
		jsonErr := model.JsonError{Code: http.StatusNotFound,
			Text: "Invalid token. Either this token was already used or a new one was generated later."}
		w.WriteHeader(jsonErr.Code)
		json.NewEncoder(w).Encode(jsonErr)
		return
	}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	if err != nil {
		panic(err)
	}

	dbUser.Password = string(hashedPassword)
	dbUser.PasswordResetToken = ""
	dbUser.Save()
	w.WriteHeader(http.StatusOK)
}
