package email

import (
	"net/smtp"

	"bitbucket.org/WillRoque/Willian_Roque_Test/api/model"
)

func SendPasswordRecoveryEmail(user model.User, body string) {
	// Set up authentication information.
	auth := smtp.PlainAuth(
		"",
		"gotestpassrecovery@gmail.com",
		"gotest123",
		"smtp.gmail.com",
	)

	mime := "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n"
	subject := "Subject: Go test password reset\n"

	// Connect to the server, authenticate, set the sender and recipient,
	// and send the email all in one step.
	err := smtp.SendMail(
		"smtp.gmail.com:587",
		auth,
		"gotestpassrecovery@gmail.com",
		[]string{user.Email},
		[]byte(subject+mime+body),
	)
	if err != nil {
		panic(err)
	}
}
