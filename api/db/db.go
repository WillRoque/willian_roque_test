package db

import (
	"database/sql"

	_ "github.com/go-sql-driver/mysql"
)

var Con *sql.DB

func Connect() {
	db, err := sql.Open("mysql", "test_user:123456@tcp(52.14.104.204:3306)/test?parseTime=true")
	if err != nil {
		panic(err)
	}

	err = db.Ping()
	if err != nil {
		panic(err)
	}

	Con = db
}
