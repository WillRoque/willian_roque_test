package routing

import (
	"net/http"

	"bitbucket.org/WillRoque/Willian_Roque_Test/api/handler"
)

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

var routes = Routes{
	Route{
		"Linkedin Login",
		"POST",
		"/linkedinLogin",
		handler.LinkedinLogin,
	},
	Route{
		"Sign-up",
		"POST",
		"/signup",
		handler.Signup,
	},
	Route{
		"Login",
		"POST",
		"/login",
		handler.Login,
	},
	Route{
		"Get User Data",
		"POST",
		"/getUserData",
		handler.GetUserData,
	},
	Route{
		"Update User",
		"POST",
		"/updateUser",
		handler.UpdateUser,
	},
	Route{
		"Password Reset",
		"POST",
		"/passwordReset",
		handler.PasswordReset,
	},
	Route{
		"Set New Password",
		"POST",
		"/setNewPassword",
		handler.SetNewPassword,
	},
}
