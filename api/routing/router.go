package routing

import (
	"net/http"

	"bitbucket.org/WillRoque/Willian_Roque_Test/api/middleware"
	"github.com/gorilla/mux"
)

func NewRouter() *mux.Router {

	router := mux.NewRouter().StrictSlash(true)
	for _, route := range routes {
		var handler http.Handler

		// Embed middlewares
		handler = route.HandlerFunc
		handler = middleware.RecoverFromPanic(handler)
		handler = middleware.SetHttpHeaders(handler)
		handler = middleware.Logger(handler, route.Name)

		router.
			Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).
			Handler(handler)
	}

	return router
}
